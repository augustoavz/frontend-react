export function GetProducts(params = {}){

    let fetchUrl = `http://api.shopear.com/ar/products/all/`;

    if (params.page === undefined) { fetchUrl = fetchUrl + `0?` } else { fetchUrl = fetchUrl + `${params.page}?` }
    if (params.query !== undefined) { fetchUrl = fetchUrl + `query=${params.query}&` }
    if (params.slug !== undefined) { fetchUrl = fetchUrl + `slug=${params.slug}&` }
    if (params.discount !== undefined) { fetchUrl = fetchUrl + `discount=${params.discount}&` }
    if (params.price !== undefined) { fetchUrl = fetchUrl + `price=${params.price}&` }
    fetchUrl = fetchUrl.slice(0, -1);

    console.log(fetchUrl);
    return fetch(fetchUrl)
    .then((response) => response.json()
        .then((responseJson) => {
            return responseJson;
        })
    )
    .catch((error) => {
    console.error(error);
    });
}

export function GetFeatured(params = {}){

    let fetchUrl = `http://api.shopear.com/ar/products/featured/`;

    if (params.page === undefined) { fetchUrl = fetchUrl + `0` } else { fetchUrl = fetchUrl + `${params.page}` }

    console.log(fetchUrl);
    return fetch(fetchUrl)
    .then((response) => response.json()
        .then((responseJson) => {
            return responseJson;
        })
    )
    .catch((error) => {
    console.error(error);
    });
}

export function GetDiscount(params = {}){

    let fetchUrl = `http://api.shopear.com/ar/products/discount/`;

    if (params.page === undefined) { fetchUrl = fetchUrl + `0` } else { fetchUrl = fetchUrl + `${params.page}` }

    console.log(fetchUrl);
    return fetch(fetchUrl)
    .then((response) => response.json()
        .then((responseJson) => {
            return responseJson;
        })
    )
    .catch((error) => {
    console.error(error);
    });
}

export function GetProductsByStore(params = {}){

    let fetchUrl = `http://api.shopear.com/ar/products/store/`;

    if (params.storeId !== undefined) { fetchUrl = fetchUrl + `${params.storeId}/` }
    if (params.page === undefined) { fetchUrl = fetchUrl + `0` } else { fetchUrl = fetchUrl + `${params.page}/` }

    console.log(fetchUrl);
    return fetch(fetchUrl)
    .then((response) => response.json()
        .then((responseJson) => {
            return responseJson;
        })
    )
    .catch((error) => {
    console.error(error);
    });
}

export function GetProductsById(params = {}){

    let fetchUrl = `http://api.shopear.com/ar/products/`;

    if (params.productId !== undefined) { fetchUrl = fetchUrl + `${params.productId}/` }

    console.log(fetchUrl);
    return fetch(fetchUrl)
    .then((response) => response.json()
        .then((responseJson) => {
            return responseJson;
        })
    )
    .catch((error) => {
    console.error(error);
    });
}

export function GetCategories(){

    let fetchUrl = `http://api.shopear.com/ar/helpers/get/categories/`;

    console.log(fetchUrl);
    return fetch(fetchUrl)
    .then((response) => response.json()
        .then((responseJson) => {
            return responseJson;
        })
    )
    .catch((error) => {
    console.error(error);
    });
}

export function GetStores(params = {}){
    let fetchUrl = `http://api.shopear.com/ar/stores/search/`;

    if (params.page === undefined) { fetchUrl = fetchUrl + `0` } else { fetchUrl = fetchUrl + `${params.page}`}
    if (params.page !== undefined) { fetchUrl = fetchUrl + `?query=${params.query}` }

    console.log(fetchUrl);
    return fetch(fetchUrl)
    .then((response) => response.json()
        .then((responseJson) => {
            return responseJson;
        })
    )
    .catch((error) => {
    console.error(error);
    });    
}

export function GetFeaturedStores(){
    let fetchUrl = `http://api.shopear.com/ar/stores/featured`;

    console.log(fetchUrl);
    return fetch(fetchUrl)
    .then((response) => response.json()
        .then((responseJson) => {
            return responseJson;
        })
    )
    .catch((error) => {
    console.error(error);
    });    
}

export function GetStoreBySlug(params = {}){
    let fetchUrl = `http://api.shopear.com/ar/stores/`;

    if (params.slug !== undefined) { fetchUrl = fetchUrl + params.slug }

    console.log(fetchUrl);
    return fetch(fetchUrl)
    .then((response) => response.json()
        .then((responseJson) => {
            return responseJson;
        })
    )
    .catch((error) => {
    console.error(error);
    });    
}

export function GetUserByQuery(params = {}){
    let fetchUrl = `http://api.shopear.com/ar/users/search/`;

    if (params.page === undefined) { fetchUrl = fetchUrl + `0` } else { fetchUrl = fetchUrl + `${params.page}`}
    if (params.page !== undefined) { fetchUrl = fetchUrl + `?query=${params.query}` }

    console.log(fetchUrl);
    return fetch(fetchUrl)
    .then((response) => response.json()
        .then((responseJson) => {
            return responseJson;
        })
    )
    .catch((error) => {
    console.error(error);
    });    
}

export function GetUser(params = {}){
    let fetchUrl = `http://api.shopear.com/ar/users/`;

    if (params.query !== undefined) { fetchUrl = fetchUrl + params.query}

    console.log(fetchUrl);
    return fetch(fetchUrl)
    .then((response) => response.json()
        .then((responseJson) => {
            return responseJson;
        })
    )
    .catch((error) => {
    console.error(error);
    });    
}